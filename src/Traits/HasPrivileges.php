<?php

namespace Dmitriydalmar\LaravelRolesPrivileges\Traits;

use Dmitriydalmar\LaravelRolesPrivileges\Models\Privilege;
use Dmitriydalmar\LaravelRolesPrivileges\Models\Role;

/**
 * @property-read \Illuminate\Database\Eloquent\Collection|\Dmitriydalmar\LaravelRolesPrivileges\Models\Privilege[] $privileges
 */
trait HasPrivileges
{
    use HasRoles;

    public function privileges()
    {
        return $this->belongsToMany(Role::class)
            ->leftJoin('privilege_role', 'privilege_role.role_id', 'roles.id')
            ->leftJoin('privileges', 'privileges.id', 'privilege_role.privilege_id')
            ->select('privileges.*');
    }

    public function hasPrivilege($title)
    {
        return !!$this->privileges->firstWhere('title', $title);
    }
}
